FROM golang:latest as builder

WORKDIR /build
ADD . /build/

RUN mkdir /tmp/cache
RUN CGO_ENABLED=0 GOCACHE=/tmp/cache go build -o /build/travel_control

FROM scratch

ENV TRAVEL_HOME=/opt/travel_control \
    PATH=$TRAVEL_HOME:$PATH
    
WORKDIR $TRAVEL_HOME

COPY --from=builder /build/travel_control $TRAVEL_HOME/
COPY html $TRAVEL_HOME/html

ENTRYPOINT ["/opt/travel_control/travel_control"]
